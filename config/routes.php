<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::prefix('admin', function($routes){
    $routes->connect('/', ['controller' => 'Admin', 'action' => 'dashboard']);

    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/signin', ['controller' => 'Users', 'action' => 'signin']);

    $routes->connect('/users', ['controller' => 'Users', 'action' => 'index']);
    $routes->connect('/users/add', ['controller' => 'Users', 'action' => 'add']);

    $routes->connect('/categories', ['controller' => 'Categories', 'action' => 'index']);
    $routes->connect('/categories/add', ['controller' => 'Categories', 'action' => 'add']);
    $routes->connect('/categories/edit', ['controller' => 'Categories', 'action' => 'edit']);
    $routes->connect('/categories/delete', ['controller' => 'Categories', 'action' => 'delete']);

    $routes->connect('/articles', ['controller' => 'Articles', 'action' => 'index']);
    $routes->connect('/articles/add', ['controller' => 'Articles', 'action' => 'add']);
    $routes->connect('/articles/edit', ['controller' => 'Articles', 'action' => 'edit']);
    $routes->connect('/articles/del-image', ['controller' => 'Articles', 'action' => 'delImage']);

    $routes->connect('/files', ['controller' => 'Files', 'action' => 'index']);
    $routes->connect('/files/add', ['controller' => 'Files', 'action' => 'add']);
    $routes->connect('/files/edit', ['controller' => 'Files', 'action' => 'edit']);

    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));
    $routes->applyMiddleware('csrf');

    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'index']);
    $routes->connect('/', ['controller' => 'Articles', 'action' => 'index']);

    $routes->fallbacks(DashedRoute::class);
});

/**
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * Router::scope('/api', function (RouteBuilder $routes) {
 *     // No $routes->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */
