<?php ?>
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="images/icon/logo.png" alt="Admin Panel" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="active">
                    <?= $this->Html->link(
                        __('<i class="fas fa-tachometer-alt"></i> Dashboard'), 
                        ['controller' => 'Admin', 'action' => 'dashboard'],
                        ['escape' => false]
                    )?>
                </li>
                <li>
                    <?= $this->Html->link(
                        __(' <i class="fas fa-users"></i> Users'),
                        ['controller' => 'Users', 'action' => 'index'],
                        ['escape' => false]
                    )?>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i> Articles
                    </a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <?= $this->Html->link(
                            __('<i class="far fa-list-alt"></i> Categories'),
                            ['controller' => 'Categories', 'action' => 'index'],
                            ['escape' => false]
                        )?>
                        <?= $this->Html->link(
                            __('<i class="far fa-newspaper"></i> Articles'),
                            ['controller' => 'Articles', 'action' => 'index'],
                            ['escape' => false]
                        )?>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>