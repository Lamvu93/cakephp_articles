<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-2">
            <?= $this->Flash->render() ?>
        </div>

        <div class="col-md-8 offset-2">
            <?= $this->Form->create($category) ?>
            <div class="card">
                <div class="card-header">
                    <strong>Add New Categories</strong>
                </div>
                <div class="card-body card-block">
                    <?php
                    $this->Form->setTemplates([
                        'input' => '<input class="form-control" type="{{type}}" name="{{name}}"{{attrs}} />',
                        'error' => '<small class="form-text text-danger"> {{content}} {{error}} </small>'
                    ]);
                    ?>
                    <div class="form-group">
                        <label for="nf-email" class=" form-control-label">Name:</label>
                        <?= $this->Form->control('name', ['label' => false, 'name' => 'name', 'required' => false, 'type' => 'text', 'placeholder' => 'Please enter name', 'value' => $category->name]) ?>
                    </div>
                    <div class="form-group">
                        <label for="nf-password" class=" form-control-label">Slug:</label>
                        <?= $this->Form->control('slug', ['label' => false, 'name' => 'slug', 'required' => false, 'type' => 'text', 'placeholder' => 'Please enter slug', 'value' => $category->slug]) ?>
                    </div>

                </div>
                <div class="card-footer">
                    <?= $this->Form->button(
                        __('<i class="fa fa-dot-circle-o"></i> Add new'),
                        ['class' => 'btn btn-primary btn-sm', 'escape' => false]
                    )?>

                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
