<?php ?>
<div class="row">
    <div class="col-md-12">
        <?= $this->Flash->render() ?>
    </div>

    <div class="col-md-12">
        <?= $this->Form->create($data)?>
        <div class="card">
            <div class="card-header">
                <strong>Add New User</strong>
            </div>

            <div class="card-body card-block">
                <?php

                $this->Form->setTemplates([
                    'input' => '<input class="form-control" type="{{type}}" name="{{name}}"{{attrs}} />',
                    'error' => '<small class="form-text text-danger"> {{content}} {{error}} </small>'
                ]);
                ?>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="lastname">Username</label>
                    </div>
                    <div class="col-md-9">
                        <?= $this->Form->input('username', ['label' => false, 'name' => 'username', 'required' => false, 'type' => 'text', 'placeholder' => 'Please enter username']) ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="email">Email</label>
                    </div>
                    <div class="col-md-9">
                        <?= $this->Form->input('email', ['label' => false, 'name' => 'email', 'required' => false, 'type' => 'email', 'placeholder' => 'Please enter email']) ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="password">Password</label>
                    </div>
                    <div class="col-md-9">
                        <?= $this->Form->input('password', ['label' => false, 'name' => 'password', 'required' => false, 'type' => 'password', 'placeholder' => 'Please enter password']) ?>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <?= $this->Form->button(
                    __('<i class="fa fa-dot-circle-o"></i> Add new'),
                    ['class' => 'btn btn-primary btn-sm', 'escape' => false]
                )?>

                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>

        </div>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-md-12">

    </div>
</div>