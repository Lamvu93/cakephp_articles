<!DOCTYPE html>
<html lang="en">
	<head>
		<?= $this->Html->charset() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="au theme template">
	    <meta name="author" content="Lam Vu">
	    <meta name="keywords" content="au theme template">

		<title><?= $title ?></title>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

		<!-- Load nhung meta, css, script tu nhung trang khac vao day -->
		<?php
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>

	    <!-- Fontfaces CSS-->
	    <?= $this->Html->css('/assets/admin/css/font-face.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css') ?>

	    <!-- Bootstrap CSS-->
		<?= $this->Html->css('/assets/bootstrap/css/bootstrap.min.css') ?>

	    <!-- Vendor CSS-->
	    <?= $this->Html->css('/assets/admin/vendor/animsition/animsition.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/wow/animate.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/css-hamburgers/hamburgers.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/slick/slick.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/select2/select2.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.css') ?>

	    <!-- Main CSS-->
	    <?= $this->Html->css('/assets/admin/css/theme.css') ?>

	     <!-- Jquery JS-->
		<?= $this->Html->script('jquery-3.3.1.min.js') ?>

	</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                	<?= $this->Flash->render() ?>
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="images/icon/logo.png" alt="CoolAdmin">
                            </a>
                        </div>
                        <div class="login-form">
                        	<?php
								$this->Form->setTemplates([
									'input' => '<input class="au-input au-input--full" type="{{type}}" name="{{name}}"{{attrs}}/>'
								]);
							?>

                            <?= $this->Form->create(null, [
								'url' => ['controller' => 'Users', 'action' => 'login'],
								'autocomplete' => 'off',
                                'id' => 'loginForm'
							]) ?>
                                <div class="form-group">
									<?= $this->Form->input('email', ['class' => 'form-control', 'placeholder' => 'Enter email']) ?>
								</div>
								<div class="form-group">
									<?= $this->Form->input('password', ['class' => 'form-control', 'type' => 'password', 'placeholder' => 'Enter password']) ?>
								</div>

                                <div class="login-checkbox">
                                    <label>
                                        <input type="checkbox" name="remember">Remember Me
                                    </label>
                                    <label>
                                        <a href="#">Forgotten Password?</a>
                                    </label>
                                </div>

                                <?= $this->Form->button('Sign In', ['class' => 'au-btn au-btn--block au-btn--green m-b-20', 'id' => 'btnLogin', 'type' => 'submit']) ?>

                            <?= $this->Form->end() ?>
                            <div class="register-link">
                                <p>
                                    Don't you have account?
                                    <?= $this->Html->link('Sign Up Here', 
                                    	['controller' => 'Users', 'action' => 'signup']
                                	) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap JS-->
	<?= $this->Html->script('/assets/bootstrap/js/bootstrap.bundle.min.js') ?>
	<?= $this->Html->script('/assets/admin/vendor/bootstrap-4.1/popper.min.js') ?>
	<?= $this->Html->script('/assets/bootstrap/js/bootstrap.min.js') ?>

	<!-- Vendor JS       -->
	<?= $this->Html->script('/assets/admin/vendor/slick/slick.min.js') ?>
	<?= $this->Html->script('/assets/admin/vendor/wow/wow.min.js') ?>
	<?= $this->Html->script('/assets/admin/vendor/animsition/animsition.min.js') ?>
	<?= $this->Html->script('/assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>

	<?= $this->Html->script('/assets/admin/vendor/counter-up/jquery.waypoints.min.js') ?>
	<?= $this->Html->script('/assets/admin/vendor/counter-up/jquery.counterup.min.js') ?>

	<?= $this->Html->script('/assets/admin/vendor/circle-progress/circle-progress.min.js') ?>
	<?= $this->Html->script('/assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js') ?>
	<?= $this->Html->script('/assets/admin/vendor/chartjs/Chart.bundle.min.js') ?>
	<?= $this->Html->script('/assets/admin/vendor/select2/select2.min.js') ?>

	<!-- Main JS-->
	<?= $this->Html->script('/assets/admin/js/main.js') ?>

	<?= $this->fetch('script') ?>

    <script>
        var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
        $(document).ready(function(){

            $('#btnLogin').click(function(){
                var email = $('#email').val();
                var password = $('#password').val();


                //$.ajax({
                //    type: 'POST',
                //    headers: {
                //        'X-CSRF-Token': csrfToken
                //    },
                //    url: '<?//= $this->Url->build(['controller' => 'Users', 'action' => 'checklogin']) ?>//',
                //    data: {email: email, password: password},
                //    success: function(res){
                //        console.log(res);
                //        // if (response.redirect) {
                //        //     // data.redirect contains the string URL to redirect to
                //        //     window.location.href = data.redirect;
                //        // }
                //    }
                //});
            });
        });
    </script>

</body>

</html>