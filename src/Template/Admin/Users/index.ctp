<?php ?>
<div class="row">
    <div class="col-md-12">
        <?php echo $this->Flash->render(); ?>
        <?php
        // echo "<pre>";
        // print_r($users);
        // echo "</pre>";
        ?>
    </div>
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">Users</h3>
            <div class="table-data__tool">
                <div class="table-data__tool-left">
                    <div class="rs-select2--light rs-select2--md">
                        <select class="js-select2" name="property">
                            <option selected="selected">All Properties</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <div class="rs-select2--light rs-select2--sm">
                        <select class="js-select2" name="time">
                            <option selected="selected">Today</option>
                            <option value="">3 Days</option>
                            <option value="">1 Week</option>
                        </select>
                        <div class="dropDownSelect2"></div>
                    </div>
                    <button class="au-btn-filter">
                        <i class="zmdi zmdi-filter-list"></i> filters
                    </button>
                </div>
                <div class="table-data__tool-right">
                    <?= $this->Html->link(
                        __('<i class="zmdi zmdi-plus"></i> add new'),
                        ['action' => 'add'],
                        ['class' => 'au-btn au-btn-icon au-btn--green au-btn--small', 'escape' => false]
                    )?>
                </div>
            </div>
            <form method="post">
                <div>
                    <button type="submit" class="btn btn-info" formaction="<?= $this->Url->build(['action' => 'deleteselected']) ?>">
                        <i class="fas fa-trash"></i> Delete Selected
                    </button>
                </div>

                <div class="table-responsive table-responsive-data2">
                    <table class="table table-data2">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="checkall">
                            </th>
                            <th>username</th>
                            <th>email</th>
                            <th>trạng thái</th>
                            <th>ngày tạo</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty(iterator_count($users))){ ?>
                            <?php foreach($users as $user){ ?>
                                <tr class="tr-shadow">
                                    <td>
                                        <input type="checkbox" class="checkbox" name="ids[]" value="<?= $user->id ?>">
                                    </td>
                                    <td><?= h($user->username) ?></td>
                                    <td>
                                        <span class="block-email"><?= h($user->email) ?></span>
                                    </td>
                                    <td>
                                        <span class="status--process">Processed</span>
                                    </td>
                                    <td><?= h($user->created->format('d-m-Y H:i:s')) ?></td>
                                    <td>
                                        <div class="table-data-feature">
                                            <?= $this->Html->link(
                                                __('<i class="zmdi zmdi-more"></i>'),
                                                ['action' => 'view', $user->id],
                                                ['class' => 'item', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'View', 'escape' => false]
                                            )?>
                                            <?= $this->Html->link(
                                                __('<i class="zmdi zmdi-edit"></i>'),
                                                ['action' => 'edit', $user->id],
                                                ['class' => 'item', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Edit', 'escape' => false]
                                            )?>
                                            <?= $this->Form->postLink(
                                                __('<i class="zmdi zmdi-delete"></i>'),
                                                ['action' => 'delete', $user->id],
                                                ['class' => 'item', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Delete', 'escape' => false, 'confirm' => __('Are you sure you want to delete '.$user->username.'?', $user->id)]
                                            )?>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="spacer"></tr>
                            <?php } ?>
                        <?php }else{ ?>
                            <tr class="tr-shadow">
                                <td colspan="6" class="text-center">
                                    Không tìm thấy dữ liệu.
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                    <!-- PAGINATE -->
                    <?php
                    // Custom html paginate
                    $paginator = $this->Paginator->setTemplates([
                        'number' => '<li class="page-item"> <a class="page-link" href="{{url}}"> {{text}} </a> </li>',
                        'current' => '<li class="page-item active"> <a class="page-link" href="{{url}}"> {{text}} </a> </li>',
                        'first' => '<li class="page-item"> <a class="page-link" href="{{url}}"> << </a> </li>',
                        'last' => '<li class="page-item"> <a class="page-link" href="{{url}}"> >> </a> </li>',
                        'prevActive' => '<li class="page-item"> <a class="page-link" href="{{url}}"> < </a> </li>',
                        'nextActive' => '<li class="page-item"> <a class="page-link" href="{{url}}"> > </a> </li>'
                    ]);
                    ?>
                    <div class="paginator mt-3">
                        <ul class="pagination">
                            <?php
                            echo $paginator->first();

                            if($paginator->hasPrev()){
                                echo $paginator->prev();
                            }

                            echo $paginator->numbers();

                            if($paginator->hasNext()){
                                echo $paginator->next();
                            }

                            echo $paginator->last();
                            ?>
                        </ul>
                        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>
                    <!-- END PAGINATE -->
                </div>
            </form>
            <!-- END DATA TABLE -->
    </div>
</div>

<script>
    $('#checkall').click(function(){
        $('.checkbox').prop('checked', $(this).prop('checked'));
    });

    // Neu nhu check het thi checkbox checkall cung se duoc check
    $('.checkbox').click(function(){
        var total = $('.checkbox').length;
        var number = $('.checkbox:checked').length;

        if(total == number){
            $('#checkall').prop('checked', true);
        }else{
            $('#checkall').prop('checked', false);
        }
    });
</script>