<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<style>
    .wrap-img{
        position: relative;

    }
    .wrap-img .btn-del-img{
        display: block;
        position: absolute;
        top: 5px;
        right: 20px;
        background: #dc3545;
        color: #fff;
        font-size: 14px;
        width: 20px;
        height: 20px;
        line-height: 20px;
        text-align: center;

    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $this->Flash->render() ?>
        </div>

        <div class="col-md-12">
            <?= $this->Form->create($article, ['type' => 'file']) ?>
            <div class="card">
                <div class="card-header">
                    <strong>Edit Articles</strong>
                </div>
                <div class="card-body card-block">
                    <?php
                    $this->Form->setTemplates([
                        'input' => '<input class="form-control" type="{{type}}" name="{{name}}"{{attrs}} />',
                        'select' => '<select class="form-control" name="{{name}}" {{attrs}}>{{content}}</select>',
                        'textarea' => '<textarea class="form-control" name="{{name}}" {{attrs}}>{{value}}</textarea>',
                        'file' => '<input type="file" name="{{name}}[]"{{attrs}}>',
                        'error' => '<small class="form-text text-danger"> {{content}} {{error}} </small>'
                    ]);
                    ?>
                    <div class="form-group">
                        <label for="nf-email" class=" form-control-label">Title:</label>
                        <?= $this->Form->control('title', ['label' => false, 'name' => 'title', 'required' => false, 'type' => 'text', 'placeholder' => 'Please enter title', 'value' => $article->title]) ?>
                    </div>
                    <div class="form-group">
                        <label for="nf-password" class=" form-control-label">Slug:</label>
                        <?= $this->Form->control('slug', ['label' => false, 'name' => 'slug', 'required' => false, 'type' => 'text', 'placeholder' => 'Please enter slug', 'value' => $article->slug]) ?>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="nf-password" class=" form-control-label">Categories:</label>
                            <?= $this->Form->control('categories_id', ['label' => false, 'type' => 'select', 'name' => 'categories_id', 'options' => $categories, 'empty' => 'Choose Categories']) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nf-password" class=" form-control-label">Content:</label>
                        <?= $this->Form->control('content', ['label' => false, 'name' => 'content', 'required' => false, 'type' => 'textarea', 'rows' => '7', 'placeholder' => 'Please enter content']) ?>
                    </div>
                    <div class="form-group">
                        <label for="nf-password" class=" form-control-label">Files:</label>
                        <?= $this->Form->file('file', ['label' => false, 'name' => 'file', 'multiple' => true]) ?>
                    </div>
                    <div class="row form-group">
                        <?php
                            foreach($article->files as $file){
                        ?>
                                <div class="col-md-2 wrap-img">
                                    <<?= $file->type ?> src="/<?= $file->path ?>" alt="<?= $article->title ?>" class="img-fluid"></<?= $file->type ?>>
                                    <a href="#" class="btn-del-img" data-id="<?= $file->id ?>" data-url="<?= $this->Url->build(['action' => 'delImage']) ?>">X</a>
                                </div>
                        <?php
                            }
                        ?>

                    </div>
                </div>
                <div class="card-footer">
                    <?= $this->Form->button(
                        __('<i class="fa fa-dot-circle-o"></i> Update'),
                        ['class' => 'btn btn-primary btn-sm', 'escape' => false]
                    )?>

                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.btn-del-img').click(function(){
            var id = $(this).data('id');
            var url = $(this).data('url');

            console.log(id);
            $.ajax({
                type: 'POST',
                url: url,
                data: {item: id},
                success: function(res){
                    if(res.result == 'success'){
                        location.reload();
                    }
                },
                error: function() {
                    console.log("Delete failed!");
                }
            });

        });
    });
</script>