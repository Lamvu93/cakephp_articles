<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
 */
?>
<div class="container">
    <div class="justify-content-center">
        <div class="row content">


        <?= $last_id = '' ?>
        <?php foreach ($articles as $article): ?>
            <?php $last_id = $article-> id ?>
            <div class="col-12 col-md-6">
                <div class="post-block <?= count($article->files) > 1 ? 'post-slide' : 'post-classic' ?>">
                    <div class="post-img <?= count($article->files) > 1 ? 'post-img-slide' : '' ?> ">
                        <?php foreach ($article->files as $file): ?>
                            <div class="<?= count($file) > 1 ? 'post-slide_img' : '' ?>">
                                <img src="/<?= $file->path ?>" alt="<?= $article->title ?>">
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="post-detail">
                        <a class="post-title regular" href="blog_detail.html"><?= $article->title ?></a>
                        <div class="post-credit">
                            <div class="author"><a class="author-avatar" href="blog_2col.html#"><img src="assets/pages/images/avatar/avatar-1.png" alt="<?= $article->user->username ?>"></a>
                                <h5 class="author-name"><?= $article->user->username ?></h5>
                            </div>
                            <h5 class="upload-day"><?= $article->created->format('F d, Y') ?></h5>
                            <div class="post-tag">
                                <?= $article->has('category') ? $this->Html->link($article->category->name, ['controller' => 'Categories', 'action' => 'view', $article->category->id]) : '' ?>
<!--                                    <a href="index.html">--><?//= $article->category->name ?><!--</a>-->
                            </div>
                        </div>
                        <p class="post-describe">
                            <?= $this->Text->truncate($article->content, 350,
                                [
                                    'ellipsis' => '...',
                                    'exact' => false
                                ])?>
                        </p>
                    </div>
                </div>

            </div>
        <?php endforeach; ?>
        </div>

        <div class="col-12 text-center">
            <button class="normal-btn" id="load-more" data-id="<?= $last_id ?>" data-page="<?= $this->request->getParam('paging')['Articles']['page'] ?>" data-page-count="<?= $this->request->getParam('paging')['Articles']['pageCount'] ?>">Load more</button>
        </div>
    </div>
</div>

<script>
    var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
    $(document).ready(function(){
        $('#load-more').click(function(){
            var last_id = $(this).data('id');

            console.log(typeof(last_id));

            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                url: '<?= $this->Url->build(['controller' => 'Articles', 'action' => 'loadMore']) ?>',
                data: {lastId: last_id},
                success: function(res){
                    console.log(res.result.length);
                    if(res.result.length > 0){
                        $.each(res.result, function () {
                            var string1 = `<div class="col-12 col-md-6">
                                        <div class="post-block post-classic">
                                            <div class="post-img post-img-slide">`;


                            var string2 = $.each(this.files, function (index, value) {
                                console.log(value.path);
                                `<div class="post-slide-img">
                                                        <img src="${value.path}" alt="">
                                                 </div>`
                            });
                            var string3 = `</div>
                                            <div class="post-detail">
                                                <a class="post-title regular" href="blog_detail.html">${this.title}</a>
                                                <div class="post-credit">
                                                    <div class="author">
                                                        <a class="author-avatar" href="blog_2col.html#">
                                                            <img src="assets/pages/images/avatar/avatar-1.png" alt="">
                                                        </a>
                                                        <h5 class="author-name">${this.user.username}</h5>
                                                    </div>
                                                    <h5 class="upload-day">${this.created}</h5>
                                                    <div class="post-tag">
                                                        <a href="index.html">${this.category.name}</a>
                                                    </div>
                                                </div>
                                                <p class="post-describe">${this.content}</p>
                                            </div>
                                        </div>
                                    </div>`;
                            var all = string1 + string2 + string3;
                            $('.content').append(all);
                        });
                    }
                },
                error: function(){

                }
            });
        });
    });
</script>