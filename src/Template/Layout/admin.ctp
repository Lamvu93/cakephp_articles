<!DOCTYPE html>
<html lang="en">
	<head>
		<?= $this->Html->charset() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="au theme template">
	    <meta name="author" content="Lam Vu">
	    <meta name="keywords" content="au theme template">

		<title><?= $title ?></title>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

	    <!-- Fontfaces CSS-->
	    <?= $this->Html->css('/assets/admin/css/font-face.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css') ?>

	    <!-- Bootstrap CSS-->
		<?= $this->Html->css('/assets/bootstrap/css/bootstrap.min.css') ?>

	    <!-- Vendor CSS-->
	    <?= $this->Html->css('/assets/admin/vendor/animsition/animsition.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/wow/animate.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/css-hamburgers/hamburgers.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/slick/slick.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/select2/select2.min.css') ?>
	    <?= $this->Html->css('/assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.css') ?>

	    <!-- Main CSS-->
	    <?= $this->Html->css('/assets/admin/css/theme.css') ?>

	    <!-- Load nhung meta, css, script tu nhung trang khac vao day -->
		<?php
		echo $this->fetch('meta');
		echo $this->fetch('css');
		?>

	     <!-- Jquery JS-->
		<?= $this->Html->script('jquery-3.3.1.min.js') ?>

	</head>
	<body class="animsition">
	    <div class="page-wrapper">
	        <!-- HEADER MOBILE-->
	       	<?= $this->element('Admin/header_mobile') ?>
	        <!-- END HEADER MOBILE-->

	        <!-- MENU SIDEBAR-->
	        <?= $this->element('Admin/navbar') ?>
	        <!-- END MENU SIDEBAR-->

	        <!-- PAGE CONTAINER-->
	        <div class="page-container">
	            <!-- HEADER DESKTOP-->
	            <?= $this->element('Admin/header_desktop') ?>
	            <!-- HEADER DESKTOP-->

	            <!-- MAIN CONTENT-->
	            <div class="main-content">
	                <div class="section__content section__content--p30">
	                    <div class="container-fluid">
	                    	<?= $this->fetch('content') ?>
							
							<!-- FOOTER -->
	                    	<div class="row">
							    <div class="col-md-12">
							        <div class="copyright">
							            <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
							        </div>
							    </div>
							</div>
							<!-- END FOOTER -->
	                    </div>
	                </div>
	            </div>
	            <!-- END MAIN CONTENT-->
	            <!-- END PAGE CONTAINER-->
	        </div>

	    </div>

	   

		<!-- Bootstrap JS-->
		<?= $this->Html->script('/assets/bootstrap/js/bootstrap.bundle.min.js') ?>
		<?= $this->Html->script('/assets/admin/vendor/bootstrap-4.1/popper.min.js') ?>
		<?= $this->Html->script('/assets/bootstrap/js/bootstrap.min.js') ?>

		<!-- Vendor JS       -->
		<?= $this->Html->script('/assets/admin/vendor/slick/slick.min.js') ?>
		<?= $this->Html->script('/assets/admin/vendor/wow/wow.min.js') ?>
		<?= $this->Html->script('/assets/admin/vendor/animsition/animsition.min.js') ?>
		<?= $this->Html->script('/assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>

		<?= $this->Html->script('/assets/admin/vendor/counter-up/jquery.waypoints.min.js') ?>
		<?= $this->Html->script('/assets/admin/vendor/counter-up/jquery.counterup.min.js') ?>

		<?= $this->Html->script('/assets/admin/vendor/circle-progress/circle-progress.min.js') ?>
		<?= $this->Html->script('/assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js') ?>
		<?= $this->Html->script('/assets/admin/vendor/chartjs/Chart.bundle.min.js') ?>
		<?= $this->Html->script('/assets/admin/vendor/select2/select2.min.js') ?>

		<!-- Main JS-->
		<?= $this->Html->script('/assets/admin/js/main.js') ?>

		<?= $this->fetch('scriptBottom') ?>

	</body>
</html>