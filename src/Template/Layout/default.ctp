<!DOCTYPE html>
<html lang="en">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="keywords" content="blog, business, clean, clear, cooporate, creative, design web, flat, marketing, minimal, portfolio, shop, shopping, unique">
    <meta name="author" content="PISEN | Deer Creative Theme">
    <title>
        <?= $title ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('/assets/pages/css/style.css') ?>
    <?= $this->Html->css('/assets/pages/css/slick.css') ?>
    <?= $this->Html->css('/assets/pages/css/jquery-ui.css') ?>
    <?= $this->Html->css('/assets/pages/css/custom_bootstrap.css') ?>
    <?= $this->Html->css('/assets/pages/css/fontawesome.css') ?>
    <?= $this->Html->css('/assets/pages/css/elegant.css') ?>
    <?= $this->Html->css('/assets/pages/css/plyr.css') ?>
    <?= $this->Html->css('/assets/pages/css/aos.css') ?>
    <?= $this->Html->css('/assets/pages/css/animate.css') ?>
    <?= $this->Html->css('/assets/pages/css/themify-icons.css') ?>
    <link rel="shortcut icon" href="assets/pages/images/shortcut_logo.png">
    <?= $this->Html->script('/assets/pages/js/jquery-3.4.0.min.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


</head>
<body>
    <div id="main">
        <?= $this->element('Page/header') ?><!--End header-->

        <section class="posts blog-2col">
            <?= $this->fetch('content') ?>
        </section><!--End posts-->

        <?= $this->element('Page/instagram') ?><!--End instagram-->

        <?= $this->element('Page/footer') ?><!--End footer-->

        <!-- JS file -->

        <?= $this->Html->script('/assets/pages/js/jquery-ui.min.js') ?>
        <?= $this->Html->script('/assets/pages/js/slick.min.js') ?>
        <?= $this->Html->script('/assets/pages/js/plyr.min.js') ?>
        <?= $this->Html->script('/assets/pages/js/aos.js') ?>
        <?= $this->Html->script('/assets/pages/js/jquery.scrollUp.min.js') ?>
        <?= $this->Html->script('/assets/pages/js/masonry.pkgd.min.js') ?>
        <?= $this->Html->script('/assets/pages/js/imagesloaded.pkgd.min.js') ?>
        <?= $this->Html->script('/assets/pages/js/numscroller-1.0.js') ?>
        <?= $this->Html->script('/assets/pages/js/jquery.countdown.min.js') ?>
        <?= $this->Html->script('/assets/pages/js/main.js') ?>

    </div>
</body>
</html>