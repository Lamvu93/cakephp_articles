<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        //Sử dụng lớp Auth cho đăng nhập
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    // Những trường dùng để đăng nhập (email và password) *
                    // username và password là những trường của hệ thống
                    'fields' => ['username' => 'email', 'password' => 'password'],
                    'userModel' => 'Users'
                ]
            ],
            // Route login action
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            //Login thành công sẽ trả về trang dashboard.
            'loginRedirect' => [
                'controller' => 'Admin',
                'action' => 'dashboard'
            ],
            //Logout sẽ trả về trang login.
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'authError' => 'Đăng nhập trước khi sử dụng dịch vụ này của chúng tôi.',
            'authorize' => 'Controller',
            'storage' => 'Session'
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeFilter(Event $event){
        // Cho phep su dung khi chua dang nhap
        $this->Auth->allow(['logout']);
    }

    /**
     * Cấp quyền được vào trang admin panel
     * @author Black
     * @param  [type]  $user [description]
     * @return boolean       [description]
     */
    public function isAuthorized($user = null)
    {
        // Bất kì user nào cũng có thể xem được những trang public
        if (!$this->request->getParam('prefix')) {
            return true;
        }

        // Chỉ những user có role = 1 mới có quyền vào Admin Panel
        if ($this->request->getParam('prefix') === 'admin') {
            //return (bool)($user['role'] === 1);
            return true;
        }

        // Default deny
        return false;
    }




}
