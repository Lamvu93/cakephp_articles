<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 *
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('default');
        $this->Auth->allow(['index', 'view', 'loadMore']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->set('title', 'Articles');
        $this->paginate = [
            'contain' => [
                'Categories' => ['fields' => ['id', 'name']],
                'Users' => ['fields' => ['id', 'username']],
                'Files'
            ],
            'limit' => 2
        ];
        $query = $this->Articles->find()->order(['Articles.created' => 'DESC']);
        $articles = $this->paginate($query);

        $this->set(compact('articles'));
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Categories', 'Users']
        ]);

        $this->set('article', $article);
    }

    public function loadMore(){
        $this->request->allowMethod('post');
        $last_item = $this->request->getData('lastId');
        $query = $this->Articles->find()->where(['Articles.id <' => $last_item])->order(['Articles.created' => 'DESC']);
        $this->paginate = [
            'contain' => [
                'Categories' => ['fields' => ['id', 'name']],
                'Users' => ['fields' => ['id', 'username']],
                'Files'
            ],
            'limit' => 2
        ];
        $articles = $this->paginate($query);

        $this->response->type('json');
        $this->response->body(json_encode(array('result' => $articles)));
        return $this->response;
    }

}
