<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Http\Response;
use Cake\Event\Event;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('admin');
        $this->Auth->allow(['checklogin']);
    }

    public function index(){
        $this->set('title', 'Users');
        $query = $this->Users->find()->order('created', 'DESC');
        $this->paginate = ['limit' => 10];
        $users = $this->paginate($query);
        $this->set(compact('users'));
    }

    public function add(){
        $this->set('title', 'Add User');
        $user = $this->Users->newEntity();
        if ($this->request->is('post') && !empty($this->request->getData())) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($user->errors()){
                // co loi validate
                $this->Flash->error(__('Hãy nhập đầy đủ thông tin bên dưới.'));
            }else{
                // user khi mới đc add đều có role = 2
                $user->role = 2;

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Thêm mới user thành công.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Thêm mới không thành công. Hãy thử lại.'));
            }
        }
        $this->set('data', $user);
    }

    public function login(){
        $this->set('title', 'Login');
        $this->viewBuilder()->setLayout(false);
        $login = $this->Users->newEntity();
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Email hoặc mật khẩu không đúng'));
            }
        }
        $this->set(compact('login'));
    }

//    public function login(){
//        $this->set('title', 'Login');
//        $this->viewBuilder()->setLayout(false);
//    }
//
//    public function checklogin(){
//        $this->request->allowMethod('post');
//        $tmpUser['User']['username'] = $this->request->getData('email');
//        $tmpUser['User']['password'] = $this->request->getData('password');
//
//        if($this->Auth->setUser($tmpUser)){
//            $result = json_encode(array('result' => 'success'));
//        }
//        else{
//            $result = json_encode(array('result' => $tmpUser['User']['password']));
//        }
//        $this->response->type('json');
//        $this->response->body($result);
//        return $this->response;
//    }

    /**
     * Logout function
     * @author Black
     * @return [type] [description]
     */
    public function logout(){
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Delete multi by checkbox
     * @author Black
     * @return [type] [description]
     */
    public function deleteselected(){
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData('ids');
        foreach($data as $item){
            $this->Users->deleteAll(['id' => $item]);
        }
        return $this->redirect(['action' => 'index']);
    }



}