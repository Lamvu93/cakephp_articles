<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Http\Response;

class AdminController extends AppController
{
    public function dashboard(){
        $this->set('title', 'Dashboard');
        $this->viewBuilder()->setLayout('admin');
    }
}